const DEFAULT_PROPERTIES = {
    name: "unnamed",
    //Dynamically create date object
    get birthdate() {
        return new Date();
    }
};

class Dragon {
    constructor({ name, birthdate } = {}) {
        this.name = name || DEFAULT_PROPERTIES.name;
        this.birthdate = birthdate || DEFAULT_PROPERTIES.birthdate;
    }
}

module.exports = Dragon;