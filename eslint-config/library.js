/**
 * @author      Rijnhard Hessel
 * @copyright   Copyright (c) 2016 Platforma.io */

module.exports = {
    extends: [
        './rules/base'
    ].map(require.resolve),

    // explicitly list all used env configs for readability
    // http://eslint.org/docs/user-guide/configuring#specifying-environments
    env: {
        es6                  : true,
        node                 : true,
        'shared-node-browser': true,
        mocha                : false,
        browser              : false,
        commonjs             : false,
        worker               : false,
        jquery               : false,
        amd                  : false,
        jasmine              : false
    },

    // http://eslint.org/docs/user-guide/configuring#configuring-rules
    rules: {

    }
};
