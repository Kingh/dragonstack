module.exports = {
    /*
     * http://eslint.org/docs/user-guide/configuring
     * Turns out we don't need eslint-babel for ES6, since Espree already does it all.
     * We do however need it for other features
     */
    // parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion : 6,
        sourceType  : 'module',
        ecmaFeatures: {
            experimentalObjectRestSpread: false
        }
    },
    plugins: [
        'filenames' // https://www.npmjs.com/package/eslint-plugin-filenames
        // eslint-plugin-vue for vuejs
        // what about chai? could be some plugins, eslint-plugin-chai, eslint-plugin-chai-expect
    ],
    extends: [
        'eslint:recommended',
        'airbnb-base'
    ],

    // explicitly list all used env configs for readability
    // http://eslint.org/docs/user-guide/configuring#specifying-environments
    env: {
        es6                  : true,
        node                 : true,
        'shared-node-browser': true,
        mocha                : false,
        browser              : false,
        commonjs             : false,
        worker               : false,
        jquery               : false,
        amd                  : false,
        jasmine              : false
    },

    // http://eslint.org/docs/rules/
    settings: {
        'import/resolver': {
            node: { // https://www.npmjs.com/package/eslint-import-resolver-node
                extensions     : ['.js', '.es6', '.es', '.json'],
                moduleDirectory: [
                    'node_modules',
                    'src'
                ]
            }
        }
    },

    // http://eslint.org/docs/user-guide/configuring#configuring-rules
    rules: {

        // AirBnB setting: require all requires be top-level
        // http://eslint.org/docs/rules/global-require
        // disable in cases its used for optimization
        'global-require': 2,

        //---------------
        // Platforma Custom
        //---------------
        'comma-dangle'        : [2, 'never'], // because always-multiline is overkill
        //---------------
        // https://github.com/eslint/eslint/issues/6400
        //---------------
        'no-extra-parens'     : [2, 'functions'],
        // to make it easier to know what parameters are available in callabcks.
        'no-unused-vars'      : [2, { vars: 'all', args: 'none' }],
        // would like to only allow _before for neginning of variables but no option for that
        'no-underscore-dangle': 0,

        //---------------
        // https://github.com/eslint/eslint/issues/6513
        //---------------
        // 'no-extra-parens': [2, 'all', {
        //     conditionalAssign      : false,
        //     returnAssign           : false,
        //     nestedBinaryExpressions: false
        // }],

        // this allows us to check inheritance
        'no-prototype-builtins': 0,
        'no-param-reassign'    : 0,

        'valid-jsdoc': [1, { // http://eslint.org/docs/rules/valid-jsdoc
            prefer: {
                returns: 'return'
            },
            preferType: {
                String: 'string',
                object: 'Object'
            },
            requireReturn: false
        }],
        'require-jsdoc': [2, {
            require: {
                FunctionDeclaration: true,
                MethodDefinition   : true,
                ClassDeclaration   : true
            }
        }],
        curly                 : [2, 'all'],
        'default-case'        : 2,
        'dot-location'        : [2, 'property'],
        'dot-notation'        : 1,
        eqeqeq                : [2, 'smart'],
        'no-alert'            : 2,
        'no-invalid-this'     : 2,
        'no-restricted-syntax': [
            2,
            'DebuggerStatement',
            // allow ForIn, disabled in AirBnb
            'LabeledStatement',
            'WithStatement'
        ],
        'no-void'              : 2,
        'no-constant-condition': [1, {
            checkLoops: false
        }],
        'no-use-before-define': [2, {
            functions: false,
            classes  : true
        }],
        // 'brace-style': [2, '1tbs', { allowSingleLine: false }],
        'linebreak-style'     : [2, 'unix'],
        'max-nested-callbacks': [2, { max: 3 }],
        // useless. makes modules more difficult because of chicken and egg with class declaration
        'one-var'             : 0, // [2, 'always']

        'function-paren-newline': [2, 'multiline'],

        //---------------
        // indentation
        //---------------
        indent: [1, 4, {
            SwitchCase        : 1,
            VariableDeclarator: { var: 1, let: 1 }
        }],
        // http://eslint.org/docs/rules/key-spacing
        'key-spacing': [2, {
            singleLine: {
                beforeColon: false,
                afterColon : true
            },
            multiLine: {
                beforeColon: false,
                afterColon : true,
                align      : 'colon'
            }
        }],
        'no-multi-spaces': [1, {
            exceptions: {
                Property          : true,
                VariableDeclarator: true,
                ImportDeclaration : true
            }
        }],
        // stolen from airbnb, changed to warn
        // http://eslint.org/docs/rules/spaced-comment
        'spaced-comment': [1, 'always', {
            exceptions: ['-', '+'],
            markers   : ['=', '!'] // space here to support sprockets directives
        }],

        'newline-after-var'          : [2, 'always'],
        'newline-before-return'      : 2,
        'operator-linebreak'         : [2, 'before'],
        'no-implicit-globals'        : 2,
        'no-label-var'               : 2,
        'import/newline-after-import': 2,

        // specify the maximum length of a line in your program
        // http://eslint.org/docs/rules/max-len
        'max-len': [2, 120, 2, {
            ignoreUrls            : true,
            ignoreComments        : false,
            ignoreStrings         : false,
            ignoreTemplateLiterals: false
        }],

        //---------------
        // Complexity
        //---------------
        complexity      : [1, 8],
        'max-statements': [1, 10],
        'max-params'    : [1, 4],

        //---------------
        // ES6
        //---------------
        'import/extensions': [2, {
            js  : 'never',
            es  : 'never',
            es6 : 'never',
            json: 'always'
        }],
        // Forbid the use of extraneous packages
        // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-extraneous-dependencies.md
        'import/no-extraneous-dependencies': [2, {
            devDependencies     : false,
            optionalDependencies: false
        }],
        'import/no-named-as-default'       : 2,
        'import/no-deprecated'             : 2,
        'import/no-namespace'              : 0,
        'import/no-named-as-default-member': 2,

        //---------------
        // Plugins
        //---------------

        // plugin:filenames
        'filenames/match-exported': 2
    }
};
